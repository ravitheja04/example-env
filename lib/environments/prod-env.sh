#!/usr/bin/env bash

set -ex
if [[ ! -z "$API_PROJECT_SCHEME" ]] ;
then
  envman add --key PROJECT_SCHEME --value "$API_PROJECT_SCHEME"
fi